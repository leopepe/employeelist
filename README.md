## Build tools & versions used
The core tools for gradle that I used were the following:

id 'com.android.application' version '8.0.1' apply false
id 'com.android.library' version '8.0.1' apply false
id 'org.jetbrains.kotlin.android' version '1.7.20' apply false

Besides that, I used Android Studio Flamingo (2022.2.1 Patch 1)
And built the UI using compose.

## Steps to run the app
1. Unpack the provided ZIP file
2. Go to File -> Open and select the unpacked source folder
3. Sync the Gradle Files
4. Run the app using Android Studio

No special instructions/steps should be needed.

## What areas of the app did you focus on?
I mostly tried to keep a well-structured project that could be easily scalable, thus I focused on the 
Domain and Service packages

## What was the reason for your focus? What problems were you trying to solve?
If the core structure of the project is well-built, expanding/maintaining the code would be easier, 
for example, the domain package is super clean, the DTOs could change and given the appropriate mapper
is updated, the rest of the app would not notice that the data source endpoint was changed.
Also in the service package, it is now easy to add an extra source of data, for example a local DB, as
it'd only take to inject the local source to the repository and the UI/Domain layers wouldn't notice.

## How long did you spend on this project?
Around 5 hours

## Did you make any trade-offs for this project? What would you have done differently with more time?
Yes.
Given more time I would probably improve the UI a bit since I only provided custom colors for the light theme.
Also the structure I had in mind to render the UI didn't initially consider long team names, making it look a bit
inconsistent because some team names take two lines of text. The UI looks and works good otherwise.
Another thing I would improve could be making better use of Gson to handle malformed entries in the back-end response,
In the current implementation, I use a try-catch block to try and map the DTO to the local domain model and if an
exception occurs, I map the whole NetworkResponse to an Error and pass it along.
The code works and I don't think it's bad but I could certainly move to another 3rd party converter library that
integrates better with Kotlin's nullability, for example Moshi. 

## What do you think is the weakest part of your project?
I think the Theming isn't the best, colors may not work great with each other and the dark theme is outright the default
given by android, so definitely working on Theming would be a very welcome addition.
Also in general the UI/UX could definitely be improved, though I must admit, I'm a better developer than a UX designer :P 

## Did you copy any code or dependencies? Please make sure to attribute them here!
I mostly used official documentation to add/use libraries that I hadn't used before (Coil for example).

## Is there any other information you’d like us to know?
In Lyft we use a custom in-house framework wrapping Android, so it has been a while since I last did a "vanilla" Android
project from scratch. It was pretty fun for a change :P
Another small thing, I had some issues trying to update the gradle dependencies to the latest and greatest due to some weird
class duplication problems, in the end I decided to use whatever Android Studio gave me that was compatible and used the latest
versions referenced in 'quick start' docs for the extra libraries I added. 
