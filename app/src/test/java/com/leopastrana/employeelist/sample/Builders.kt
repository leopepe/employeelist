package com.leopastrana.employeelist.sample

import com.leopastrana.employeelist.domain.Employee
import com.leopastrana.employeelist.domain.EmployeeDTO
import com.leopastrana.employeelist.domain.EmployeesResponse
import com.leopastrana.employeelist.domain.EmployeesResponseDTO

object Builders {

    object Primitives {
        const val uuid: String = "123-14223"
        const val fullName: String = "Full Name"
        const val phoneNumber: String = "(419) 485-4729"
        const val emailAddress: String = "random@squareup.com"
        const val biography: String = "No bio"
        const val photoUrlSmall: String = "www.google.com"
        const val photoUrlLarge: String = "www.notgoogle.com"
        const val team: String = "Core XP"
    }

    fun makeEmployee(
        uuid: String = Primitives.uuid,
        fullName: String = Primitives.fullName,
        phoneNumber: String? = Primitives.phoneNumber,
        emailAddress: String = Primitives.emailAddress,
        biography: String? = Primitives.biography,
        photoUrlSmall: String? = Primitives.photoUrlSmall,
        photoUrlLarge: String? = Primitives.photoUrlLarge,
        team: String = Primitives.team,
        employeeType: Employee.Type = Employee.Type.FULL_TIME,
    ): Employee {
        return Employee(
            uuid = uuid,
            fullName = fullName,
            phoneNumber = phoneNumber,
            emailAddress = emailAddress,
            biography = biography,
            photoUrlSmall = photoUrlSmall,
            photoUrlLarge = photoUrlLarge,
            team = team,
            employeeType = employeeType,
        )
    }

    fun makeEmployeeDTO(
        uuid: String = Primitives.uuid,
        fullName: String = Primitives.fullName,
        phoneNumber: String? = Primitives.phoneNumber,
        emailAddress: String = Primitives.emailAddress,
        biography: String? = Primitives.biography,
        photoUrlSmall: String? = Primitives.photoUrlSmall,
        photoUrlLarge: String? = Primitives.photoUrlLarge,
        team: String = Primitives.team,
        employeeType: EmployeeDTO.Type = EmployeeDTO.Type.FULL_TIME,
    ): EmployeeDTO {
        return EmployeeDTO(
            uuid = uuid,
            fullName = fullName,
            phoneNumber = phoneNumber,
            emailAddress = emailAddress,
            biography = biography,
            photoUrlSmall = photoUrlSmall,
            photoUrlLarge = photoUrlLarge,
            team = team,
            employeeType = employeeType,
        )
    }

    fun makeEmployeesResponse(
        employees: List<Employee> = listOf(makeEmployee()),
    ): EmployeesResponse {
        return EmployeesResponse(
            employees = employees,
        )
    }

    fun makeEmployeesResponseDTO(
        employees: List<EmployeeDTO> = listOf(makeEmployeeDTO()),
    ): EmployeesResponseDTO {
        return EmployeesResponseDTO(
            employees = employees,
        )
    }
}