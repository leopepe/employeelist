package com.leopastrana.employeelist.service

import com.google.common.truth.Truth.assertThat
import com.leopastrana.employeelist.sample.Builders
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@OptIn(ExperimentalCoroutinesApi::class)
class EmployeeRepositoryTest {

    private val remoteDataSource: RemoteDataSource = mock()

    private val employeeRepository: EmployeeRepository = EmployeeRepository(
        remoteDataSource = remoteDataSource,
    )

    @Test
    fun test_getEmployees() = runTest {
        whenever(remoteDataSource.getEmployees()).thenReturn(
            NetworkResult.Success(Builders.makeEmployeesResponseDTO())
        )

        val employees = employeeRepository.getEmployees().first()

        if (employees is NetworkResult.Success) {
            assertThat(employees.data).containsExactly(Builders.makeEmployee())
            assert(true)
        } else {
            assert(false)
        }
        verify(remoteDataSource, times(1)).getEmployees()
    }
}