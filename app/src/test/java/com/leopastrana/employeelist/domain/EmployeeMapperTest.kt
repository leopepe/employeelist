package com.leopastrana.employeelist.domain

import com.google.common.truth.Truth.assertThat
import com.leopastrana.employeelist.sample.Builders
import org.junit.Test

class EmployeeMapperTest {

    @Test
    fun test_EmployeeDTO_toEmployee() {
        val actual = Builders.makeEmployeeDTO().toEmployee()
        val expected = Builders.makeEmployee()

        assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun test_EmployeeDTOType_toEmployeeType() {
        val actual = EmployeeDTO.Type.FULL_TIME.toEmployeeType()
        val expected = Employee.Type.FULL_TIME

        assertThat(actual).isEqualTo(expected)
    }
}
