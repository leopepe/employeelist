package com.leopastrana.employeelist.domain

import com.google.common.truth.Truth.assertThat
import com.leopastrana.employeelist.sample.Builders
import org.junit.Test

class EmployeesResponseMapperTest {

    @Test
    fun test_EmployeesResponseDTO_toEmployeesResponse() {
        val actual = Builders.makeEmployeesResponseDTO().toEmployeesResponse()
        val expected = Builders.makeEmployeesResponse()

        assertThat(actual).isEqualTo(expected)
    }
}
