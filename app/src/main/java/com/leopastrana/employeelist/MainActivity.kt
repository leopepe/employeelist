package com.leopastrana.employeelist

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.leopastrana.employeelist.ui.employeelist.EmployeeListScreen
import com.leopastrana.employeelist.ui.navigation.Routes
import com.leopastrana.employeelist.ui.theme.EmployeeListTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            EmployeeListTheme {
                val navController = rememberNavController()
                NavHost(
                    navController = navController,
                    startDestination = Routes.EMPLOYEE_LIST,
                ) {
                    composable(
                        route = Routes.EMPLOYEE_LIST,
                    ) {
                        EmployeeListScreen()
                    }
                }
            }
        }
    }
}
