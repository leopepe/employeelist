package com.leopastrana.employeelist.ui.di

import com.leopastrana.employeelist.service.retrofit.EmployeeRetrofitService
import com.leopastrana.employeelist.service.retrofit.NetworkResultAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RetrofitModule {

    const val BASE_URL = "https://s3.amazonaws.com/sq-mobile-interview/"

    @Provides
    @Singleton
    fun provideEmployeeRetrofitService(): EmployeeRetrofitService {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(NetworkResultAdapterFactory())
            .build()
            .create(EmployeeRetrofitService::class.java)
    }
}