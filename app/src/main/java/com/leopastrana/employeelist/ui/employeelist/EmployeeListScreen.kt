package com.leopastrana.employeelist.ui.employeelist

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.calculateEndPadding
import androidx.compose.foundation.layout.calculateStartPadding
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.pullrefresh.PullRefreshIndicator
import androidx.compose.material.pullrefresh.pullRefresh
import androidx.compose.material.pullrefresh.rememberPullRefreshState
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.leopastrana.employeelist.R

@OptIn(ExperimentalMaterial3Api::class, ExperimentalMaterialApi::class)
@Composable
fun EmployeeListScreen(
    viewModel: EmployeeListViewModel = hiltViewModel(),
) {
    val state = viewModel.getState().collectAsState()
    val pullRefreshState = rememberPullRefreshState(
        refreshing = state.value.isLoading,
        onRefresh = { viewModel.fetchEmployees() },
    )

    Scaffold { paddingValues ->
        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(
                    start = paddingValues.calculateStartPadding(LocalLayoutDirection.current),
                    top = paddingValues.calculateTopPadding(),
                    end = paddingValues.calculateEndPadding(LocalLayoutDirection.current),
                    bottom = paddingValues.calculateBottomPadding(),
                )
                .pullRefresh(pullRefreshState),
        ) {
            if (state.value.error != null) {
                ErrorView(
                    error = state.value.error!!,
                    onTryAgainPressed = { viewModel.fetchEmployees() },
                )
            } else if (state.value.employees != null) {
                val employees = state.value.employees!!
                LazyColumn(
                    modifier = Modifier
                        .fillMaxSize(),
                    verticalArrangement = Arrangement.spacedBy(16.dp),
                    contentPadding = PaddingValues(16.dp)
                ) {
                    if (employees.isNotEmpty()) {
                        items(employees) { employee ->
                            EmployeeListItem(
                                employee = employee,
                            )
                        }
                    } else {
                        item {
                            EmptyView(
                                onTryAgainPressed = { viewModel.fetchEmployees() }
                            )
                        }
                    }
                }
            }
            PullRefreshIndicator(
                modifier = Modifier.align(Alignment.TopCenter),
                refreshing = state.value.isLoading,
                state = pullRefreshState,
            )
        }
    }
}

@Composable
internal fun ErrorView(
    error: EmployeeListState.EmployeeListError,
    onTryAgainPressed: () -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Text(
            modifier = Modifier
                .padding(16.dp),
            text = when (error) {
                EmployeeListState.EmployeeListError.NetworkError -> {
                    stringResource(id = R.string.network_error)
                }
                EmployeeListState.EmployeeListError.OtherError -> {
                    stringResource(id = R.string.other_error)
                }
                is EmployeeListState.EmployeeListError.ServerError -> {
                    stringResource(id = R.string.server_error, error.code)
                }
            },
            fontWeight = FontWeight.Light,
            fontStyle = FontStyle.Normal,
            fontSize = 18.sp,
        )
        Button(
            onClick = onTryAgainPressed
        ) {
            Text(
                text = stringResource(id = R.string.try_again),
            )
        }
    }
}

@Composable
internal fun EmptyView(
    onTryAgainPressed: () -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Text(
            modifier = Modifier
                .padding(16.dp),
            text = stringResource(id = R.string.no_employees),
            fontWeight = FontWeight.Light,
            fontStyle = FontStyle.Normal,
            fontSize = 18.sp,
        )
        Button(
            onClick = onTryAgainPressed
        ) {
            Text(
                text = stringResource(id = R.string.try_again),
            )
        }
    }
}
