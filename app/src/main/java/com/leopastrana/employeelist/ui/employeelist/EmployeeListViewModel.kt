package com.leopastrana.employeelist.ui.employeelist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.leopastrana.employeelist.service.EmployeeRepository
import com.leopastrana.employeelist.service.NetworkResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class EmployeeListViewModel @Inject constructor(
    private val employeeRepository: EmployeeRepository,
) : ViewModel() {

    private val state: MutableStateFlow<EmployeeListState> = MutableStateFlow(EmployeeListState())

    fun getState(): StateFlow<EmployeeListState> = state

    init {
        fetchEmployees()
    }

    fun fetchEmployees() {
        viewModelScope.launch {
            state.value = state.value.copy(
                employees = null,
                isLoading = true,
                error = null,
            )
            employeeRepository.getEmployees().collect { result ->
                when (result) {
                    is NetworkResult.NetworkError -> state.value = state.value.copy(
                        employees = null,
                        isLoading = false,
                        error = EmployeeListState.EmployeeListError.NetworkError,
                    )
                    is NetworkResult.ServerError -> state.value = state.value.copy(
                        employees = null,
                        isLoading = false,
                        error = EmployeeListState.EmployeeListError.ServerError(result.code),
                    )
                    is NetworkResult.Success -> state.value = state.value.copy(
                        employees = result.data,
                        isLoading = false,
                        error = null,
                    )
                    is NetworkResult.UnknownError -> state.value = state.value.copy(
                        employees = null,
                        isLoading = false,
                        error = EmployeeListState.EmployeeListError.OtherError,
                    )
                }
            }
        }
    }
}