package com.leopastrana.employeelist.ui.theme

import androidx.compose.ui.graphics.Color

val yellowish = Color(0xFFFFF5E0)
val pinkish = Color(0xFFFFE6D5)
