package com.leopastrana.employeelist.ui.employeelist

import com.leopastrana.employeelist.domain.Employee

data class EmployeeListState(
    val employees: List<Employee>? = null,
    val isLoading: Boolean = true,
    val error: EmployeeListError? = null,
) {
    sealed interface EmployeeListError {
        data class ServerError(val code: Int) : EmployeeListError
        object NetworkError : EmployeeListError
        object OtherError : EmployeeListError
    }
}