package com.leopastrana.employeelist.ui.di

import com.leopastrana.employeelist.service.RemoteDataSource
import com.leopastrana.employeelist.service.retrofit.EmployeeRetrofitService
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class AppModule {

    @Binds
    abstract fun bindRemoteDataSource(
        employeeRetrofitService: EmployeeRetrofitService,
    ): RemoteDataSource
}