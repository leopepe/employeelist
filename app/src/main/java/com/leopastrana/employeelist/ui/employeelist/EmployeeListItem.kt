package com.leopastrana.employeelist.ui.employeelist

import androidx.annotation.StringRes
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import coil.request.CachePolicy
import coil.request.ImageRequest
import com.leopastrana.employeelist.R
import com.leopastrana.employeelist.domain.Employee
import com.leopastrana.employeelist.ui.theme.EmployeeListTheme

@Composable
fun EmployeeListItem(
    employee: Employee,
    modifier: Modifier = Modifier,
) {
    Row(
        modifier = modifier
            .clip(RoundedCornerShape(16.dp))
            .height(IntrinsicSize.Min)
            .background(MaterialTheme.colorScheme.secondaryContainer)
            .fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Column(
            modifier = Modifier
                .fillMaxHeight()
                .padding(end = 4.dp)
                .weight(5F),
            verticalArrangement = Arrangement.SpaceBetween,
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            Surface(
                modifier = Modifier.fillMaxWidth(),
                shape = RoundedCornerShape(
                    topStart = 16.dp,
                    topEnd = 0.dp,
                    bottomEnd = 16.dp,
                    bottomStart = 0.dp,
                ),
            ) {
                Text(
                    modifier = Modifier.padding(4.dp),
                    text = employee.team,
                    style = MaterialTheme.typography.labelSmall,
                    textAlign = TextAlign.Center,
                )
            }
            AsyncImage(
                model = ImageRequest.Builder(LocalContext.current)
                    .diskCachePolicy(CachePolicy.ENABLED)
                    .data(employee.photoUrlSmall)
                    .crossfade(enable = true)
                    .build(),
                contentDescription = stringResource(
                    id = R.string.profile_picture
                ),
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .padding(8.dp)
                    .clip(CircleShape)
                    .size(50.dp),
            )
            Surface(
                modifier = Modifier.fillMaxWidth(),
                shape = RoundedCornerShape(
                    topStart = 0.dp,
                    topEnd = 16.dp,
                    bottomEnd = 0.dp,
                    bottomStart = 16.dp,
                ),
            ) {
                Text(
                    modifier = Modifier.padding(4.dp),
                    text = stringResource(employee.employeeType.toDisplayResourceId()),
                    style = MaterialTheme.typography.labelSmall,
                    textAlign = TextAlign.Center,
                )
            }
        }
        Column(
            modifier = Modifier
                .fillMaxHeight()
                .padding(4.dp)
                .weight(10F),
            verticalArrangement = Arrangement.SpaceBetween,
        ) {
            Column {
                Text(
                    text = employee.fullName,
                    style = MaterialTheme.typography.titleMedium,
                )
                Text(
                    text = employee.emailAddress,
                    style = MaterialTheme.typography.labelSmall,
                )
            }
            Text(
                modifier = Modifier.padding(bottom = 4.dp),
                text = employee.biography.orEmpty(),
                style = MaterialTheme.typography.bodySmall,
                fontWeight = FontWeight.Light,
            )
        }
    }
}

@StringRes
fun Employee.Type.toDisplayResourceId(): Int {
    return when (this) {
        Employee.Type.FULL_TIME -> R.string.full_time
        Employee.Type.PART_TIME -> R.string.part_time
        Employee.Type.CONTRACTOR -> R.string.contractor
    }
}

@Preview(showBackground = true)
@Composable
fun EmployeeListItemPreview() {
    EmployeeListTheme {
        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .background(Color.White)
                .padding(all = 16.dp),
            verticalArrangement = Arrangement.spacedBy(16.dp)
        ) {
            items(5) {
                EmployeeListItem(
                    employee = Employee(
                        uuid = "123",
                        fullName = "Leo Pastrana",
                        phoneNumber = "3313979806",
                        emailAddress = "leopepe97@gmail.com",
                        biography = "Random bio to test",
                        photoUrlSmall = "https://s3.amazonaws.com/sq-mobile-interview/photos/b44629e2-47e0-459a-a936-4683f783536b/small.jpg",
                        photoUrlLarge = "https://s3.amazonaws.com/sq-mobile-interview/photos/b44629e2-47e0-459a-a936-4683f783536b/large.jpg",
                        team = "Vehicles",
                        employeeType = Employee.Type.FULL_TIME,
                    )
                )
            }
        }
    }
}
