package com.leopastrana.employeelist

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class EmployeeListApplication : Application()
