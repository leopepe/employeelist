package com.leopastrana.employeelist.domain

fun EmployeeDTO.toEmployee(): Employee {
    return Employee(
        uuid = uuid,
        fullName = fullName,
        phoneNumber = phoneNumber,
        emailAddress = emailAddress,
        biography = biography,
        photoUrlSmall = photoUrlSmall,
        photoUrlLarge = photoUrlLarge,
        team = team,
        employeeType = employeeType.toEmployeeType(),
    )
}

fun EmployeeDTO.Type.toEmployeeType(): Employee.Type {
    return when (this) {
        EmployeeDTO.Type.FULL_TIME -> Employee.Type.FULL_TIME
        EmployeeDTO.Type.PART_TIME -> Employee.Type.PART_TIME
        EmployeeDTO.Type.CONTRACTOR -> Employee.Type.CONTRACTOR
    }
}
