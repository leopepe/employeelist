package com.leopastrana.employeelist.domain

data class EmployeesResponse(
    val employees: List<Employee>,
)
