package com.leopastrana.employeelist.domain

fun EmployeesResponseDTO.toEmployeesResponse(): EmployeesResponse {
    return EmployeesResponse(
        employees = employees.map {
            it.toEmployee()
        },
    )
}
