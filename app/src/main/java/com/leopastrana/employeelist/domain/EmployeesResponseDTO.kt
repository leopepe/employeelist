package com.leopastrana.employeelist.domain

import com.google.gson.annotations.SerializedName

data class EmployeesResponseDTO(
    @SerializedName("employees")
    val employees: List<EmployeeDTO>
)
