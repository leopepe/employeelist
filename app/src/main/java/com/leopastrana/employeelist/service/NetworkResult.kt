package com.leopastrana.employeelist.service

sealed class NetworkResult<out T> {
    class Success<T: Any>(val data: T) : NetworkResult<T>()
    class ServerError<T: Any>(val code: Int, val message: String?) : NetworkResult<T>()
    class NetworkError<T: Any> : NetworkResult<T>()
    class UnknownError<T: Any> : NetworkResult<T>()

    fun <X: Any> mapSuccess(
        successMapper: (T) -> NetworkResult<X>,
    ): NetworkResult<X> = when (this) {
        is NetworkError -> NetworkError()
        is ServerError -> ServerError(code, message)
        is Success -> successMapper(data)
        is UnknownError -> UnknownError()
    }
}
