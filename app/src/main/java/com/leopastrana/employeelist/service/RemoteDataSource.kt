package com.leopastrana.employeelist.service

import com.leopastrana.employeelist.domain.EmployeesResponseDTO

interface RemoteDataSource {

    suspend fun getEmployees(): NetworkResult<EmployeesResponseDTO>
}
