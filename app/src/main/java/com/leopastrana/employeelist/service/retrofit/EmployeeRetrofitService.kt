package com.leopastrana.employeelist.service.retrofit

import com.leopastrana.employeelist.domain.EmployeesResponseDTO
import com.leopastrana.employeelist.service.NetworkResult
import com.leopastrana.employeelist.service.RemoteDataSource
import retrofit2.http.GET

interface EmployeeRetrofitService : RemoteDataSource {

    @GET("employees.json")
    override suspend fun getEmployees(): NetworkResult<EmployeesResponseDTO>
}
