package com.leopastrana.employeelist.service.retrofit

import com.leopastrana.employeelist.service.NetworkResult
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Retrofit
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

class NetworkResultAdapterFactory : CallAdapter.Factory() {
    override fun get(
        returnType: Type,
        annotations: Array<out Annotation>,
        retrofit: Retrofit,
    ): CallAdapter<*, *>? {
        if (returnType !is ParameterizedType) {
            return null
        }

        val containerType = getParameterUpperBound(0, returnType)
        if (getRawType(containerType) != NetworkResult::class.java) {
            return null
        }

        if (containerType !is ParameterizedType) {
            return null
        }

        val successBodyType = getParameterUpperBound(0, containerType)

        return when (getRawType(returnType)) {
            Call::class.java -> {
                NetworkResultAdapter<Any>(successBodyType)
            }
            else -> null
        }
    }
}
