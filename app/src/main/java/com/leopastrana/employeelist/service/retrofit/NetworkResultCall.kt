package com.leopastrana.employeelist.service.retrofit

import com.leopastrana.employeelist.service.NetworkResult
import okhttp3.Request
import okio.IOException
import okio.Timeout
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

internal class NetworkResultCall<T>(
    private val delegate: Call<T>,
) : Call<NetworkResult<T>> {

    override fun clone(): Call<NetworkResult<T>> {
        return NetworkResultCall(delegate.clone())
    }

    override fun execute(): Response<NetworkResult<T>> {
        return Response.success(NetworkResult.Success(delegate.execute().body()!!))
    }

    override fun isExecuted(): Boolean {
        return delegate.isExecuted
    }

    override fun cancel() {
        delegate.cancel()
    }

    override fun isCanceled(): Boolean {
        return delegate.isCanceled
    }

    override fun request(): Request {
        return delegate.request()
    }

    override fun timeout(): Timeout {
        return delegate.timeout()
    }

    override fun enqueue(callback: Callback<NetworkResult<T>>) {
        delegate.enqueue(
            object : Callback<T> {
                override fun onResponse(call: Call<T>, response: Response<T>) {
                    callback.onResponse(
                        this@NetworkResultCall,
                        Response.success(
                            response.code(),
                            if (response.isSuccessful) NetworkResult.Success(response.body()!!)
                            else NetworkResult.ServerError(response.code(), response.message()),
                        )
                    )
                }

                override fun onFailure(call: Call<T>, t: Throwable) {
                    callback.onResponse(
                        this@NetworkResultCall,
                        Response.success(
                            when (t) {
                                is IOException -> NetworkResult.NetworkError()
                                else -> NetworkResult.UnknownError()
                            }
                        )
                    )
                }
            }
        )
    }
}
