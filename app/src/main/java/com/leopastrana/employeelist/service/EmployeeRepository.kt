package com.leopastrana.employeelist.service

import com.leopastrana.employeelist.domain.Employee
import com.leopastrana.employeelist.domain.toEmployeesResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class EmployeeRepository @Inject constructor(
    private val remoteDataSource: RemoteDataSource,
) {
    fun getEmployees(): Flow<NetworkResult<List<Employee>>> {
        return flow {
            emit(
                remoteDataSource.getEmployees().mapSuccess {
                    try {
                        val employeesResponse = it.toEmployeesResponse()
                        NetworkResult.Success(employeesResponse.employees)
                    } catch (e: Exception) {
                        NetworkResult.UnknownError()
                    }
                }
            )
        }
    }
}
