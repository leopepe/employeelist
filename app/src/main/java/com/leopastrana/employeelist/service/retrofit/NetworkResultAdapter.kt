package com.leopastrana.employeelist.service.retrofit

import com.leopastrana.employeelist.service.NetworkResult
import retrofit2.Call
import retrofit2.CallAdapter
import java.lang.reflect.Type

internal class NetworkResultAdapter<T>(
    private val type: Type,
) : CallAdapter<T, Call<NetworkResult<T>>> {
    override fun responseType(): Type = type

    override fun adapt(call: Call<T>): Call<NetworkResult<T>> {
        return NetworkResultCall(call)
    }
}
